#!/usr/bin/env node
const path = require("path");
const fs = require("fs");
const os = require("os");
const parseArgs = require("minimist");
const toml = require("toml");

const globalFilePath = path.resolve(os.homedir(), ".ra.toml");

const ensureFile = () => {
  if (!fs.existsSync(globalFilePath)) {
    fs.writeFileSync(globalFilePath, "", {encoding: "utf-8"});
  }
  return fs.readFileSync(globalFilePath, {encoding: "utf-8"});
}

const main = (args) => {
  const parsedArgs = parseArgs(args);
  if (parsedArgs.edit) {
    console.log(globalFilePath);
    return 0;
  }
  if (parsedArgs._[0]) {
    const config = toml.parse(ensureFile());
    let currentConfig = {children: config};
    let query = parsedArgs._[0].split("/");
    while (query.length > 0) {
      currentConfig = (Object.entries(currentConfig.children || {}).find(([k,]) => k.startsWith(query[0])) || {})[1];
      query = query.slice(1);
    }
    if (currentConfig) {
      console.log(currentConfig.path);
      return 0;
    } else {
      console.error(`No projects match query ${parsedArgs._[0]}`);
      console.log(".");
      return 1;
    }
  }
}

process.exit(main(process.argv.slice(2)));