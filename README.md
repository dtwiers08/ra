# RepoAddict

Quickly navigate all your projects & repos without typing so much!

## What it does

RepoAddict is for anyone who has a ton of software projects and can never seem to keep track of them all. Either that, or your poor soul works in a lot of very verbosely named Java projects and you don't feel like typing all that to `cd` into directories.

### Example
Let's say you have projects `foo`, `bar`, and `ReallyReallyLongNamedProject`, and say, `bar` is a monorepo in which you have a project `baz`. To visualize:
```
- foo
- bar
  |- baz
- ReallyReallyLongNamedProject
```
Really, those 3 top level projects could be anywhere in your file system - they don't have to be right next to each other. In any case, you would configure your config file (using `vim $(ra --edit)` or whatever you want to use for editing). It's a standard TOML file:
```toml
[foo]
path = "/absolute/path/to/foo/repo"

[bar]
path = "/absolute/path/to/bar/repo"

[bar.children.baz]
path = "/absolute/path/to/baz/subproject"

[rrlnp]
path = "/absolute/path/to/ReallyReallyLongNamedProject"
description = "ReallyReallyLongNamedProject - some Java project or something"
```
Let's say you're using Bash or Zsh and want to "cd" into your baz project inside your bar monorepo. `cd $(ra bar/baz)` would get you there (see "Installation" about having a function that does the CD for you so it's a bit less clunky). The query logic just does a "startswith" comparison with all of the keys, so the bare minimum would actually be `cd $(ra b/)` since bar is the only one in this scenario that starts with "b" and baz is the only child.

## Configuration

See example above for the general flow of the document. The config file (accessed easily using `ra --edit`) is a [TOML](https://toml.io/) file with one big top-level table of project aliases and paths. The Alias is simply the key of the entry (what's in the square brackets), which is the unique identifier that you start typing in the "queries". There are 2 values inside each project that matter: path and children. Children allows recursing, useful for complex projects and monorepos where you may commonly work on just one deeply-nested project; and path is the absolute path of the project directory. All other attributes are ignored (you can put "name" or "description" if you like - I may add functionality with those at a later date, but for now they're simply ignored)

## Installation
```sh
npm install -g repo-addict
```

I would recommend installing a function in your favorite shell RC file (.zshrc, .bashrc, Profile.ps1 [yes this should work for powershell too]) like this:
```zsh
# bash/zsh
function racd () {
  cd $(ra "$1")
}
```
That way, in the context of the above example, you could run `racd rr` and you would immediately `cd` into your really really long named project by typing 7 characters!

## Usage

`ra [--edit] [querypath]`

* `--edit` returns the path to the configuration file for RepoAddict, so you can easily edit it in your favorite text editor (`vim $(ra --edit)`)
* `querypath` is a `/` delimited set of "queries" where for each query, you just have to type as many characters as it takes to find the given project. The next query then looks into the children of the previously matched project.
